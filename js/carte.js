$(document).ready(function() {

	$('.menufood-item.menu-soups').on('click', onSoupsClick);
	$('.menufood-item.menu-appetizer').on('click', onAppetizerClick);
	$('.menufood-item.menu-weekly-entree').on('click', onMainlyClick);
	$('.menufood-item.menu-side-dish').on('click', onSideDishClick);
	$('.menufood-item.menu-salad').on('click', onClickSalad);
	$('.menufood-item.menu-dessert').on('click', onClickDessert);
	$('.menufood-item.menu-allmenu').on('click', onAllClick);
});

function hideAll() { 
	$('.food.soups').addClass('hidden');
	$('.food.appetizer').addClass('hidden'); 
	$('.food.mainly').addClass('hidden');
	$('.food.side-dish').addClass('hidden');
	$('.food.salad').addClass('hidden');
	$('.food.dessert').addClass('hidden');
	$('.picture-container').addClass('hidden');
}

function showAll() {
	$('.food.soups').removeClass('hidden');
	$('.food.appetizer').removeClass('hidden');
	$('.food.mainly').removeClass('hidden');
	$('.food.side-dish').removeClass('hidden');
	$('.food.salad').removeClass('hidden');
	$('.food.dessert').removeClass('hidden');
}

function onSoupsClick() {
	// content handling
	hideAll();
	showSoups();

	// menu handling
	var soupElement = $('.js-soup-menu');
	setActiveMenu(soupElement);	
}

function onAppetizerClick() {
	// content handling
	hideAll();
	showAppetizer();

	// menu handling
	var appetizerElement = $('.js-appetizer-menu');
	setActiveMenu(appetizerElement);
}

function onMainlyClick() {
	// content handling
	hideAll();
	showMainly();

	// menu handling
	var weeklyElement = $('.js-weekly-menu');
	setActiveMenu(weeklyElement);
}

function onSideDishClick() {
	// content handling
	hideAll();
	showSideDish();

	// menu handling
	var sidedishElement = $('.js-sidedish-menu');
	setActiveMenu(sidedishElement);
}

function onClickSalad () {
	// content handling
	hideAll();
	showSalad();

	// menu handling
	var saladElement = $('.js-salad-menu');
	setActiveMenu(saladElement);
}

function onClickDessert() {
	// content handling
	hideAll();
	showDessert();

	// menu handling
	var dessertElement = $('.js-dessert-menu');
	setActiveMenu(dessertElement);
}

function onAllClick() {
// content handling {
	showAll();

	// menu handling
	var allElement = $('.js-all-menu');
	setActiveMenu(allElement);
}

function showSoups() {
	$('.food.soups').removeClass('hidden');
}

function showAppetizer() {
	$('.food.appetizer').removeClass('hidden');
}

function showMainly() {
	$('.food.mainly').removeClass('hidden');
}

function showSideDish() {
	$('.food.side-dish').removeClass('hidden');
}

function showSalad() {
	$('.food.salad').removeClass('hidden');
}

function showDessert() {
	$('.food.dessert').removeClass('hidden');
}

function setActiveMenu(element) {
	$('.js-soup-menu').removeClass('active');
	$('.js-appetizer-menu').removeClass('active');
	$('.js-weekly-menu').removeClass('active');
	$('.js-sidedish-menu').removeClass('active');
	$('.js-salad-menu').removeClass('active');
	$('.js-dessert-menu').removeClass('active');
	$('.js-all-menu').removeClass('active');
	element.addClass('active');
}