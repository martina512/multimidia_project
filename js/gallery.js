$(document).ready(function() {
	$('.gallery.in').on('click', onInterijerClick);
	$('.gallery.ex').on('click', onExterijerClick);
});

function onInterijerClick() {
	$('.photos-container.interier').removeClass('hidden');
	$('.photos-container.exterier').addClass('hidden');
	var inter = $('.gallery.in');
	setActiveMenu(inter);
}

function onExterijerClick() {
	$('.photos-container.interier').addClass('hidden');
	$('.photos-container.exterier').removeClass('hidden');
	var ex = $('.gallery.ex');
	setActiveMenu(ex);
}

function setActiveMenu(element) {
	// makni klasu sa svih menujeva
	$('.gallery.ex').removeClass('active');
	$('.gallery.in').removeClass('active');

	// dodaj klasu active na activeElement
	element.addClass('active');
}