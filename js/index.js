$(document).ready(function() {
	//initMap();
});

function initMap() {
	var pos = {
		lat: 46.3861107, 
		lng: 16.4212153
	};

  // Create a map object and specify the DOM element for display.
  var map = new google.maps.Map(document.getElementById('map'), {
    center: pos,
    scrollwheel: false,
    zoom: 16
  });


  // Create a marker and set its position.
  var marker = new google.maps.Marker({
    map: map,
    position: pos
  });
}




